import array as arr
listData=[10,20,30,40]
arrData=arr.array('i',[100,200,300])
print(arrData)  #array('i', [100, 200, 300])

arrData.fromlist(listData)
print(arrData)  #array('i', [100, 200, 300, 10, 20, 30, 40])
print(arrData.index(300))   #2(gives the index of given number)
arrData.insert(3,400)
print(arrData)  #array('i', [100, 200, 300, 400, 10, 20, 30, 40] (add the data at given index)

arrData.pop()
print(arrData)  #removes last element of array

arrData.remove(100)
print(arrData)  #remove tha element that 1st occurs

arrData.reverse()
print(arrData)  #array('i', [30, 20, 10, 400, 300, 200])

