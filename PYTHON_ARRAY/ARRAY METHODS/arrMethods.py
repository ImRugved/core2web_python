import array as arr
data=arr.array('i',[10,20,30,40,50])
print(arr)  #
print(data) #array('i', [10, 20, 30, 40, 50])
data.append(60) #appends only sigle value
print(data) #array('i', [10, 20, 30, 40, 50,60])
print(data.buffer_info())   #(2875626986528, 6)
print(id(data)) #1617094060656
print(data.count(30))   #1(gives count of given num that present in array)
data.extend([70,80,90])
print(data) #array('i', [10, 20, 30, 40, 50, 60, 70, 80, 90])
