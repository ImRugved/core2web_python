import array as arr
arrData=arr.array('i',[10,20,30,40,50])

for i in arrData[1:4:-1]:
    print(i)    #no output
print()
for i in arrData[4:1:-1]:
    print(i)   #50,40,30(in next lines)

print()
for i in arrData[4:6:1]:
    print(i)   #50

print()
print(arrData[4])   #50
#print(arrData[5])   #IndexError: array index out of range