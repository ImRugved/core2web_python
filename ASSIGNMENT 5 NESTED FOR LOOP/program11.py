'''
1 3 5 7
5 7 9 11
9 11 13 15
13 15 17 19
'''
for i in range(1,5):
    x=(i-1)*4+1
    for j in range(4):
        print(x, end=" ")
        x=x+2
    print()
