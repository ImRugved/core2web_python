class Data:
    pass

print(dir(object))
print("---------------------------------------------------------------------------------")
print(dir(Data))

'''
['__class__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__']
---------------------------------------------------------------------------------
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__']
'''

obj1=Data()
obj2=Data()
print(id(obj1)) #1926956883008
print(id(obj2)) #1926956882864

print(obj1==obj2)   #False

print(type(Data))   #<class 'type'>
print(type(obj1))   #<class '__main__.Data'>