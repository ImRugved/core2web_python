class Boss:
    def report(self):
        print('boss : report')

class M1(Boss):
    def report(self):
        print('m1:report')

class M2(Boss):
    def report(self):
        print('m2:report')

class M3(Boss):
    def report(self):
        print('m3:report')

class TL1(M1,M2):
    def report(self):
        print('tl1:report')

class TL2(M2):
    def report(self):
        print('tl2:report')

class Dev(TL1,TL2):
    def report(self):
        print('dev : report')

obj=Dev()
obj.report()   #dev : report
print(Dev.mro())#[<class '__main__.Dev'>, <class '__main__.TL1'>, <class '__main__.M1'>, <class '__main__.TL2'>, <class '__main__.M2'>, <class '__main__.Boss'>, <class 'object'>]