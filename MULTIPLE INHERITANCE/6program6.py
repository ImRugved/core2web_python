class Boss:
    def report(self):
        print('boss : report')

class M1(Boss):
    def report(self):
        print('m1:report')

class M2(Boss):
    def report(self):
        print('m2:report')

class M3(Boss):
    def report(self):
        print('m3:report')

class TL1(M1,M3):
    def report(self):
        print('tl1:report')

class TL2(M2,TL1):
    def report(self):
        print('tl2:report')

class Dev(TL1,TL2):
    def report(self):
        print('dev : report')

obj=Dev()
#TypeError: Cannot create a consistent method resolution
#order (MRO) for bases TL1, TL2
obj.report()   
print(Dev.mro())