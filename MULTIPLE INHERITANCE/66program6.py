class Manager:
    def project(self):
        print('in project : manager')

class TL1(Manager):
    pass

class TL2(Manager):
    pass

class Developer(TL1,TL2):
    pass


obj=Developer()
obj.project()   #in project : manager
print(Developer.mro())#[<class '__main__.Developer'>, <class '__main__.TL1'>, <class '__main__.TL2'>, <class '__main__.Manager'>, <class 'object'>]
