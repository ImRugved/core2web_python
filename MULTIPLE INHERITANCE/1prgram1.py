class Parent1:
    def dispData(self):
        print('in dispData')

class Parent2:
    def printData(self):
        print('in printData')

class Child(Parent1,Parent2):
    pass

obj=Child()
obj.dispData()  #in dispData
obj.printData() #in printData