class Manager:
    def project(self):
        print('in project : manager')

class TL1(Manager):
    pass

class TL2(Manager):
    def project(self):
        print('in project : TL2')

class Developer(TL1,TL2):
    def project(self):
        print('in project : dev')


obj=Developer()
obj.project()   #in project : dev
print(Developer.mro())#[<class '__main__.Developer'>, <class '__main__.TL1'>, <class '__main__.TL2'>, <class '__main__.Manager'>, <class 'object'>]
