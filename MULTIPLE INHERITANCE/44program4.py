class Manager:
    def fun(self):
        print('in fun manager')
    
class TL1(Manager):
    pass

class TL2(Manager):
    pass

class TL3(TL1,TL2):
    pass

obj=TL3()
obj.fun()   #in fun manager