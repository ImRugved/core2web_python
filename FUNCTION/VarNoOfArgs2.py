'''
def fun(*argv,x,y):
    print(x)
    print(y)
    print(argv)

fun(10,20)#TypeError: fun() missing 2 required keyword-only arguments: 'x' and 'y'
'''
def fun(x,y,*argv):
    print(x)
    print(y)
    print(argv)
fun(10,20,30,40)#10
                #20
                #(30, 40)
fun(10,20)#10
          #20
          #()

