class Demo:
    
    def __init__(self):
        self.x=10
        self._y=20
        self.__z=30

    def __fun(self):
        print('in fun')
        
    
obj=Demo()
print(obj.x)    #10
print(obj._y)   #20
print(obj._Demo__z) #30
obj._Demo__fun()    #in fun