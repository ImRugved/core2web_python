class Demo:
    z=30
    def __init__(self):
        self.x=10
        self.__y=20
        
    
obj=Demo()
print(obj.x)    #10
print(obj.z)    #30
print(obj._Demo__y) #20 (Name mangling)
print(dir(Demo))
print(dir(obj))
#print(obj.__y)  #AttributeError: 'Demo' object has no attribute '__y'
