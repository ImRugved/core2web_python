n = int(input("Enter a number from 0 to 6: "))

if n == 0:
    print("Monday")
elif n == 1:
    print("Tuesday")
elif n == 2:
    print("Wednesday")
elif n == 3:
    print("Thursday")
elif n == 4:
    print("Friday")
elif n == 5:
    print("Saturday")
elif n == 6:
    print("Sunday")
else:
    print("Invalid number")
