start_range = int(input("Enter start range: "))
end_range = int(input("Enter end range: "))

if start_range > end_range:
    print("Wrong input")
else:
    for i in range(start_range, end_range + 1):
        print(f"The character of ASCII value {i} is {chr(i)}")
