start = input("Enter start range: ")
end = input("Enter end range: ")

start_ord = ord(start)
end_ord = ord(end)

if start_ord < end_ord:
    for i in range(start_ord, end_ord + 1):
        print(f"The ASCII value of {chr(i)} is {i}")
else:
    print("Invalid input range. Start range must be less than end range.")
