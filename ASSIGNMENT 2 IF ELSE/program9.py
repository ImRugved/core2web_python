char1 = input("Enter the first character: ")
char2 = input("Enter the second character: ")

sum_ascii = 0

if ord(char1) % 2 == 1 and ord(char2) % 2 == 1:
    sum_ascii = ord(char1) + ord(char2)

print("The sum of ASCII values of", char1, "and", char2, "is:", sum_ascii)
