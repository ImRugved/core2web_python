def outerFun():
    print('in outer fun')


    def innerFun1():
        print('in inner fun 1')
    
    def innerFun2():
        print('in inner fun 2')

    return innerFun1,innerFun2

ret1,ret2=outerFun()
ret1()
ret2()