def decorFun(func):
    def wrapper():
        print('start wrapper')
        func()
        print('end wrapper')
    return wrapper

@decorFun
def normalFun():
    print('hello in normal fun')


#normalFun=decorFun(normalFun)
normalFun()
