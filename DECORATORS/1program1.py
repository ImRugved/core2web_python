def outerFun():
    print('in outer function')

    def innerFun1():
        print('in inner fun 2')

    def innerFun2():
        print('in inner fun 2')
    
    innerFun1()
    innerFun2()

outerFun()