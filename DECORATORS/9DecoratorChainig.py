def decorFun(func):
    def wrapper():
        print('start wrapper2')
        func()
        print('end wrapper2')
    return wrapper
def decorRun(func):
    def wrapper():
        print('start wrapper1')
        func()
        print('end wrapper1')

    return wrapper
@decorFun
@decorRun

def normalFun():
    print('in normal fun')
#normalFun=decorFun(normalFun)
#normalFun=decorFun(decorRun(normalFun))
normalFun()
'''
start wrapper2
start wrapper1
in normal fun
end wrapper1
end wrapper2
'''