def decorFun(func):
    print('in decor fun')

    def wrapper(*args):
        print('start wrapper')
        val=func(*args)
        print('end wrapper')
        return val
    return wrapper
@decorFun
def normalFun(x,y):                 
    print('in normal fun')
    return x+y
#normalFun=decorFun(normalFun)
print(normalFun(10,20))
'''
in decor fun
start wrapper
in normal fun
end wrapper
30
'''