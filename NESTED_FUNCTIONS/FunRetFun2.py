def outer(x,y):
    def inner(a,b):
        print('in inner function')
        return a+b
    print('outer function')
    print(x+y)
    return inner
retObj=outer(5,8)
innerRet=retObj(3,4)
print(retObj)   ##<function outer.<locals>.inner at 0x000002C117A6D120>