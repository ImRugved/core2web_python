def outer():
    def inner1():
        print('in inner function 1')
    def inner2():
        print('in inner function 2')
    print('in outer function')
    inner1()#in inner function 1
    inner2()#in inner function 2
outer() #in outer function
