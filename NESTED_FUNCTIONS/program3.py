def multiply(*args):
    result = 1
    for num in args:
        result=result*num
    print(type(args))   #<class 'tuple'>
    return result
    
n=multiply(2,3,4)
print(n)    #24
print(type(n))  #<class 'int'>