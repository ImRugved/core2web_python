def outer():
    def inner1(x,y):
        print("inner1")
        return x+y
    def inner2(a,b):
        print('inner 2')
        return a*b
    return inner1,inner2

inn1,inn2=outer()
ret1=inn1(10,20) #inner1
ret2=inn2(3,4)  #inner 2
print(ret1+ret2)    #42
print(inn1) #<function outer.<locals>.inner1 at 0x0000011C55F4D120>
print(inn2) #<function outer.<locals>.inner2 at 0x0000011C55F4CF40>