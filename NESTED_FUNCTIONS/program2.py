print('main function')
def outer():
    def inner1():
        print('in inner function 1')
    def inner2():
        print('in inner function 2')
    
    print('in outer function')
#outer().inner1()#AttributeError: 'NoneType' object has no attribute 'inner1'
outer()