def outer():
    def inner1():
        print("inner1")
    def inner2():
        print('inner 2')
    return inner1,inner2
#retObj=outer()
#print(retObj) #(<function outer.<locals>.inner1 at 0x000001C68B38D120>, <function outer.<locals>.inner2 at 0x000001C68B38CF40>)
#inn1,inn2=outer()
#inn1()  #inner 1
#inn2()  #inner 2

retObj=outer()
for i in retObj:
    i()