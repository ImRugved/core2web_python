class Employee:
    def __init__(self,empId,empName):
        print('in constructor')
        self.empId=empId
        self.empName=empName
    def empInfo(self):
        print(self.empId)
        print(self.empName)

emp1=Employee(12,'kanha')   #in constructor
emp2=Employee(15,'ashish')  #in constructor

emp1.empInfo()  #12 knaha
emp2.empInfo()  #15 ashish
