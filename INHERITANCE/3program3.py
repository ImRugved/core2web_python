class Parent:
    def __init__(self):
        print('in parent constructor')
        self.x=10
        self.y=20
    
    def dispData(self):
        print(self.x)
        print(self.y)
class Child(Parent):
   pass

obj1=Child()    #in parent constructor
                
obj1.dispData()    #10,20
