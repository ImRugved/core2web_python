class Parent:
    def __init__(self):
        print('in constructor')
    
    def ParentFun(self):
        print('in parent fun')
class Child(Parent):
    def __init__(self):
        print('in child constructor')

obj1=Child()    #in child cosntructor
obj1.ParentFun()    #in parent fun