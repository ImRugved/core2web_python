class Parent:
    def __init__(self):
        print('in parent constructor')
    
    def ParentFun(self):
        print('in parent fun')
class Child(Parent):
    def __init__(self):
        print('in child constructor')
        super().__init__()

obj1=Child()    #in child cosntructor
                #in parent constructor
obj1.ParentFun()    #in parent fun
