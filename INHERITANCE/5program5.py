class Parent:
    def __init__(self):
        self.x=10
        self.y=20
        print('in parent constructor')
    
    def dispParent(self):
        print(self.x)
        print(self.y)

class Child(Parent):
    def __init__(self):
        print('in child constructor')
        super().__init__()
        #super().__init__()
        self.x=30
        self.y=40
        #super().__init__()

obj=Child()
obj.dispParent()