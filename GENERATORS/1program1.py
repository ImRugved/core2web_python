def fun():
    yield 10
    yield 20
gen=fun()
print(next(gen)) # Output: 10
print(next(gen)) # Output: 20
print(next(gen)) # Error - StopIteration