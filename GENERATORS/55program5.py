def fun():
    print('start fun')
    yield 10
    yield 20
    yield 30
    yield
    print('end fun')
    yield

ret=fun()
print(next(ret))    #start fun , 10
print(next(ret))    #20
print(next(ret))    #30
print(next(ret))    #None
print(next(ret))